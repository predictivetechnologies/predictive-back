class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/api/v1/auth/$action" {
          controller = 'apiAuth'
        }

        "/api/v1/urls/$action" {
          controller = 'apiUrl'
        }

        "/api/v1/urls" {
          controller = 'apiUrl'
          action = 'index'
        }

        "/api/v1/feedback/$action" {
          controller = 'apiFeedback'
        }

        "/api/v1/questionnaire/$action" {
          controller = 'apiQuestionnaire'
        }

        "/api/v1/questions/$action" {
          controller = 'apiQuestion'
        }

        "/api/v1/questions" {
          controller = 'apiQuestion'
          action = 'index'
        }

        "/"(view:"/index")
        "500"(view:'/error')
	}
}
