import com.predictive.*
import org.apache.shiro.crypto.hash.Sha256Hash

class BootStrap {
    def grailsApplication

    def init = { servletContext ->
      if(!User.findByUsername("admin")) {
        def user = new User(username: "admin", passwordHash: new Sha256Hash("password").toHex())
        user.addToPermissions("*:*")
        user.save()
      }

      if(!Role.findByName("Respondent")) {
        def role = new Role(name: "Respondent")
        role.addToPermissions("apiUrl:random")
        role.addToPermissions("apiQuestionnaire:save")
        role.addToPermissions("apiQuestionnaire:finishUrl")
        role.addToPermissions("apiFeedback:save")
        role.save()
      }

      def securityManager = grailsApplication.mainContext.getBean("shiroSecurityManager")
      def sessionManager = grailsApplication.mainContext.getBean("predictiveSessionManager")

      if(sessionManager) {
        securityManager.sessionManager = sessionManager
      }

      grailsApplication.getArtefacts("Domain").each { dc ->
        //here we register custom marshallers declared in domain classes
        if (dc.hasProperty("marshaller")) {
          dc.clazz.marshaller()
        }
      }

    }
    def destroy = {
    }
}
