package com.predictive

import com.predictive.auth.FacebookToken
import grails.converters.JSON

import org.apache.shiro.crypto.hash.Sha256Hash
import org.apache.shiro.authc.UsernamePasswordToken
import org.apache.shiro.SecurityUtils

class ApiAuthController {

  def index() { }

  def register() {
    def userData = request.JSON

    def user = new User(username: userData.username, passwordHash: new Sha256Hash(userData.password).toHex())
    def role = Role.findByName("Respondent")
    user.addToRoles(role)
    user.save()

    def authToken = new UsernamePasswordToken(userData.username, userData.password)
    SecurityUtils.subject.login(authToken)

    def sessionId = SecurityUtils.subject.session.id

    def result = [
      token: sessionId,
      username: SecurityUtils.subject.principal
    ]

    render result as JSON
  }

  def login() {
    def loginData = request.JSON
    def authToken

    if(loginData.userID) {
      authToken = new FacebookToken(loginData.userID, loginData.accessToken)
    } else {
      authToken = new UsernamePasswordToken(loginData.username, loginData.password)
    }
    SecurityUtils.subject.login(authToken)

    def sessionId = SecurityUtils.subject.session.id

    def result = [
      token: sessionId,
      username: SecurityUtils.subject.principal
    ]

    render result as JSON
  }


}
