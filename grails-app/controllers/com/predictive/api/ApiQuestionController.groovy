package com.predictive.api

import com.predictive.*
import grails.converters.JSON

class ApiQuestionController {

  def index() {
    def questions = Question.findAllByActive(true)

    render questions as JSON
  }

}
