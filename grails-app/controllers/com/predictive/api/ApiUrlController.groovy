package com.predictive.api

import com.predictive.*
import grails.converters.JSON

import org.apache.shiro.SecurityUtils

class ApiUrlController {

    def random() {
      // def url = Url.executeQuery('from Url order by random()', [max: 1])
      def urls = []
      def answers = []

      def user = User.findByUsername(SecurityUtils.subject.principal)
      def userResponse = UserResponse.findByUserAndFinished(user, false)

      if(userResponse) {
        urls.add(userResponse.url)

        def responses = UserQuestionResponse.findAllByUserResponse(userResponse)

        answers = responses.sort {
          it.question.sortOrder
        }.collectEntries {
          def entry = [
            questionId: it.question.id,
            possibilities: it.possibilities.collect {
              it.id
            }
          ]
          [it.question.id, entry]
        }
      }

      if(urls.size() == 0) {
        def url = Url.executeQuery('from Url u where u.evaluated = false order by random()', [max: 1])
        if(url.size() > 0) {
          urls.addAll(url)
        }
      }

      if(urls.size() == 0) {
        def url = Url.executeQuery('from Url u where u not in (select r.url from UserResponse r where r.user = :user) order by random()', [user: user, max: 1])
        if(url.size() > 0) {
          urls.addAll(url)
        }
      }
      
      def data = [
        urls: urls,
        answers: answers,
        answered: UserResponse.countByUser(user)
      ]

      render data as JSON
    }

    def index() {
    }
}
