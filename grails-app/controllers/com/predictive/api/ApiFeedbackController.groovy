package com.predictive.api

import grails.converters.JSON
import org.apache.shiro.SecurityUtils

class ApiFeedbackController {

  def feedbackService

  def save() {
    def data = request.JSON

    feedbackService.save(SecurityUtils.subject.principal, data)

    render data as JSON
  }
}
