package com.predictive.api

import grails.converters.JSON
import org.apache.shiro.SecurityUtils

class ApiQuestionnaireController {

  def questionnaireService

  def save() {
    def data = request.JSON

    questionnaireService.save(SecurityUtils.subject.principal, data)

    render data as JSON
  }

  def finishUrl() {
    def data = request.JSON

    questionnaireService.finishUrl(SecurityUtils.subject.principal, data)

    render data as JSON
  }
}
