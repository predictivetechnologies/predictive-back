package com.predictive

import grails.transaction.Transactional

@Transactional
class QuestionnaireService {

  def finishUrl(def username, def data) {
    def user = User.findByUsername(username)
    def url = Url.findByUrl(data.url)

    def userResponse = UserResponse.findByUserAndUrl(user, url)
    url.evaluated = true
    userResponse.finished = true
    userResponse.save()
  }

  def save(def username, def questionnaireData) {
    def user = User.findByUsername(username)

    def url = Url.findByUrl(questionnaireData.url)
    def summary = questionnaireData.questionSummary

    def userResponse = UserResponse.findByUserAndUrl(user, url)

    if(!userResponse) {
      userResponse = new UserResponse(user: user, url: url)
      userResponse.save()
    }


    summary.each {
      def question = Question.get(it.id)
      def response = UserQuestionResponse.findByQuestionAndUserResponse(question, userResponse)

      if(!response) {
        response = new UserQuestionResponse(question: question,
          user: user,
          userResponse: userResponse)
        response.possibilities = []
      }

      response.possibilities.clear()

      def unknown = it.possibilities.any {
        it == 0
      }

      if(!unknown) {
        it.possibilities.each {
          def possibility = Possibility.get(it)
          response.addToPossibilities(possibility)
        }
        response.dontKnow = false
      } else {
        response.dontKnow = true
      }

      response.save()
    }


  }
}
