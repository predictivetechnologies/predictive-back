package com.predictive

import grails.transaction.Transactional
import org.apache.shiro.SecurityUtils

@Transactional
class FeedbackService {

  def save(def username, def data) {
    def feedback = new UserFeedBack()
    def user = User.findByUsername(username)
    feedback.properties = data
    feedback.user = user
    feedback.save()
  }
}
