databaseChangeLog = {

	changeSet(author: "iamedu (generated)", id: "1400943716419-1") {
		addColumn(tableName: "question") {
			column(name: "unknown_possibility", type: "bool", defaultValue: "true") {
				constraints(nullable: "false")
			}
		}
	}
}
