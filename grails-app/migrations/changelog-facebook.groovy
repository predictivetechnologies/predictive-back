databaseChangeLog = {

	changeSet(author: "iamedu (generated)", id: "1398503917851-1") {
		addColumn(tableName: "person") {
			column(name: "facebook_id", type: "varchar(255)")
		}
	}
}
