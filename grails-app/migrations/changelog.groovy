databaseChangeLog = {

	changeSet(author: "iamedu (generated)", id: "1398498987377-1") {
		createTable(tableName: "person") {
			column(name: "id", type: "int8") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "personPK")
			}

			column(name: "version", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "facebook_oauth", type: "varchar(255)")

			column(name: "password_hash", type: "varchar(255)")

			column(name: "username", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-2") {
		createTable(tableName: "person_permissions") {
			column(name: "user_id", type: "int8")

			column(name: "permissions_string", type: "varchar(255)")
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-3") {
		createTable(tableName: "person_roles") {
			column(name: "role_id", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "int8") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-4") {
		createTable(tableName: "possibility") {
			column(name: "id", type: "int8") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "possibilityPK")
			}

			column(name: "version", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "active", type: "bool") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "bytea") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "bytea") {
				constraints(nullable: "false")
			}

			column(name: "possibility", type: "text") {
				constraints(nullable: "false")
			}

			column(name: "question_id", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "sort_order", type: "int4") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-5") {
		createTable(tableName: "question") {
			column(name: "id", type: "int8") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "questionPK")
			}

			column(name: "version", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "active", type: "bool") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "bytea") {
				constraints(nullable: "false")
			}

			column(name: "explanation", type: "text") {
				constraints(nullable: "false")
			}

			column(name: "help", type: "text") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "bytea") {
				constraints(nullable: "false")
			}

			column(name: "multivalued", type: "bool") {
				constraints(nullable: "false")
			}

			column(name: "question", type: "text") {
				constraints(nullable: "false")
			}

			column(name: "sort_order", type: "int4") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-6") {
		createTable(tableName: "role") {
			column(name: "id", type: "int8") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "rolePK")
			}

			column(name: "version", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-7") {
		createTable(tableName: "role_permissions") {
			column(name: "role_id", type: "int8")

			column(name: "permissions_string", type: "varchar(255)")
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-8") {
		createTable(tableName: "url") {
			column(name: "id", type: "int8") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "urlPK")
			}

			column(name: "version", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "active", type: "bool") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "bytea") {
				constraints(nullable: "false")
			}

			column(name: "evaluated", type: "bool") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "bytea") {
				constraints(nullable: "false")
			}

			column(name: "url", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-9") {
		createTable(tableName: "user_question_response") {
			column(name: "id", type: "int8") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "user_questionPK")
			}

			column(name: "version", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "bytea") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "bytea") {
				constraints(nullable: "false")
			}

			column(name: "possibility_id", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "question_id", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "user_response_id", type: "int8") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-10") {
		createTable(tableName: "user_question_response_possibility") {
			column(name: "user_question_response_possibilities_id", type: "int8")

			column(name: "possibility_id", type: "int8")
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-11") {
		createTable(tableName: "user_response") {
			column(name: "id", type: "int8") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "user_responsePK")
			}

			column(name: "version", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "bytea") {
				constraints(nullable: "false")
			}

			column(name: "finished", type: "bool") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "bytea") {
				constraints(nullable: "false")
			}

			column(name: "url_id", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "int8") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-12") {
		createTable(tableName: "user_url") {
			column(name: "id", type: "int8") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "user_urlPK")
			}

			column(name: "version", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "bytea") {
				constraints(nullable: "false")
			}

			column(name: "date_finished", type: "bytea")

			column(name: "last_updated", type: "bytea") {
				constraints(nullable: "false")
			}

			column(name: "url_id", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "int8") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-13") {
		addPrimaryKey(columnNames: "user_id, role_id", tableName: "person_roles")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-29") {
		createIndex(indexName: "username_uniq_1398498987295", tableName: "person", unique: "true") {
			column(name: "username")
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-30") {
		createIndex(indexName: "name_uniq_1398498987318", tableName: "role", unique: "true") {
			column(name: "name")
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-31") {
		createIndex(indexName: "unique_question_id", tableName: "user_question_response", unique: "true") {
			column(name: "user_response_id")

			column(name: "question_id")
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-32") {
		createIndex(indexName: "unique_url_id", tableName: "user_response", unique: "true") {
			column(name: "user_id")

			column(name: "url_id")
		}
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-33") {
		createSequence(sequenceName: "hibernate_sequence")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-14") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "person_permissions", constraintName: "FK2D53969A17CBEBC", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "person", referencesUniqueColumn: "false")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-15") {
		addForeignKeyConstraint(baseColumnNames: "role_id", baseTableName: "person_roles", constraintName: "FKED8BF9535C51FADC", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "role", referencesUniqueColumn: "false")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-16") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "person_roles", constraintName: "FKED8BF95317CBEBC", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "person", referencesUniqueColumn: "false")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-17") {
		addForeignKeyConstraint(baseColumnNames: "question_id", baseTableName: "possibility", constraintName: "FKB46ED5B38265C3DC", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "question", referencesUniqueColumn: "false")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-18") {
		addForeignKeyConstraint(baseColumnNames: "role_id", baseTableName: "role_permissions", constraintName: "FKEAD9D23B5C51FADC", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "role", referencesUniqueColumn: "false")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-19") {
		addForeignKeyConstraint(baseColumnNames: "possibility_id", baseTableName: "user_question_response", constraintName: "FK5E98A50699DCE9B8", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "possibility", referencesUniqueColumn: "false")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-20") {
		addForeignKeyConstraint(baseColumnNames: "question_id", baseTableName: "user_question_response", constraintName: "FK5E98A5068265C3DC", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "question", referencesUniqueColumn: "false")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-21") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_question_response", constraintName: "FK5E98A50617CBEBC", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "person", referencesUniqueColumn: "false")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-22") {
		addForeignKeyConstraint(baseColumnNames: "user_response_id", baseTableName: "user_question_response", constraintName: "FK5E98A506662A773", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user_response", referencesUniqueColumn: "false")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-23") {
		addForeignKeyConstraint(baseColumnNames: "possibility_id", baseTableName: "user_question_response_possibility", constraintName: "FKD4367FFA99DCE9B8", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "possibility", referencesUniqueColumn: "false")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-24") {
		addForeignKeyConstraint(baseColumnNames: "user_question_response_possibilities_id", baseTableName: "user_question_response_possibility", constraintName: "FKD4367FFA6A6969B6", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user_question_response", referencesUniqueColumn: "false")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-25") {
		addForeignKeyConstraint(baseColumnNames: "url_id", baseTableName: "user_response", constraintName: "FKEEBA6E355295BE38", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "url", referencesUniqueColumn: "false")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-26") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_response", constraintName: "FKEEBA6E3517CBEBC", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "person", referencesUniqueColumn: "false")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-27") {
		addForeignKeyConstraint(baseColumnNames: "url_id", baseTableName: "user_url", constraintName: "FKF02302FB5295BE38", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "url", referencesUniqueColumn: "false")
	}

	changeSet(author: "iamedu (generated)", id: "1398498987377-28") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_url", constraintName: "FKF02302FB17CBEBC", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "person", referencesUniqueColumn: "false")
	}

  include file: 'changelog-facebook.groovy'
  include file: 'changelog-response.groovy'
  include file: 'changelog-unknown.groovy'
  include file: 'changelog-feedback.groovy'
}
