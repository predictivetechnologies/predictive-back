databaseChangeLog = {

	changeSet(author: "iamedu (generated)", id: "1398592585817-1") {
		dropForeignKeyConstraint(baseTableName: "user_question_response", baseTableSchemaName: "public", constraintName: "fk5e98a50699dce9b8")
	}

	changeSet(author: "iamedu (generated)", id: "1398592585817-2") {
		dropColumn(columnName: "possibility_id", tableName: "user_question_response")
	}
}
