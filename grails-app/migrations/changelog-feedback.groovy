databaseChangeLog = {

	changeSet(author: "iamedu (generated)", id: "1400948074063-1") {
		createTable(tableName: "user_feed_back") {
			column(name: "id", type: "int8") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "user_feed_bacPK")
			}

			column(name: "version", type: "int8") {
				constraints(nullable: "false")
			}

			column(name: "body", type: "text") {
				constraints(nullable: "false")
			}

			column(name: "email", type: "text") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "text") {
				constraints(nullable: "false")
			}

			column(name: "subject", type: "text") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "int8") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "iamedu (generated)", id: "1400948074063-2") {
		addColumn(tableName: "user_question_response") {
			column(name: "dont_know", type: "bool", defaultValue: true) {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "iamedu (generated)", id: "1400948074063-3") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_feed_back", constraintName: "FKFB658F7417CBEBC", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "person", referencesUniqueColumn: "false")
	}
}
