databaseChangeLog = {

	changeSet(author: "iamedu (generated)", id: "1414940694191-1") {
		modifyDataType(columnName: "date_created", newDataType: "timestamp", tableName: "possibility")
	}

	changeSet(author: "iamedu (generated)", id: "1414940694191-2") {
		modifyDataType(columnName: "last_updated", newDataType: "timestamp", tableName: "possibility")
	}

	changeSet(author: "iamedu (generated)", id: "1414940694191-3") {
		modifyDataType(columnName: "date_created", newDataType: "timestamp", tableName: "question")
	}

	changeSet(author: "iamedu (generated)", id: "1414940694191-4") {
		modifyDataType(columnName: "last_updated", newDataType: "timestamp", tableName: "question")
	}

	changeSet(author: "iamedu (generated)", id: "1414940694191-5") {
		modifyDataType(columnName: "date_created", newDataType: "timestamp", tableName: "url")
	}

	changeSet(author: "iamedu (generated)", id: "1414940694191-6") {
		modifyDataType(columnName: "last_updated", newDataType: "timestamp", tableName: "url")
	}

	changeSet(author: "iamedu (generated)", id: "1414940694191-7") {
		modifyDataType(columnName: "date_created", newDataType: "timestamp", tableName: "user_question_response")
	}

	changeSet(author: "iamedu (generated)", id: "1414940694191-8") {
		modifyDataType(columnName: "last_updated", newDataType: "timestamp", tableName: "user_question_response")
	}

	changeSet(author: "iamedu (generated)", id: "1414940694191-9") {
		modifyDataType(columnName: "date_created", newDataType: "timestamp", tableName: "user_response")
	}

	changeSet(author: "iamedu (generated)", id: "1414940694191-10") {
		modifyDataType(columnName: "last_updated", newDataType: "timestamp", tableName: "user_response")
	}

	changeSet(author: "iamedu (generated)", id: "1414940694191-11") {
		modifyDataType(columnName: "date_created", newDataType: "timestamp", tableName: "user_url")
	}

	changeSet(author: "iamedu (generated)", id: "1414940694191-12") {
		modifyDataType(columnName: "date_finished", newDataType: "timestamp", tableName: "user_url")
	}

	changeSet(author: "iamedu (generated)", id: "1414940694191-13") {
		modifyDataType(columnName: "last_updated", newDataType: "timestamp", tableName: "user_url")
	}
}
