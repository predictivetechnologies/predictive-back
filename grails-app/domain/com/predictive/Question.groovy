package com.predictive

import org.joda.time.DateTime

import grails.converters.JSON

class Question {

    static hasMany = [possibilities: Possibility]

    Boolean active = true
    Boolean multivalued = false
    Boolean unknownPossibility = true
    String question
    String help
    String explanation
    Integer sortOrder

    DateTime dateCreated
    DateTime lastUpdated

    static mapping = {
      question type: 'text'
      explanation type: 'text'
      help type: 'text'
      sort sortOrder: 'asc'
    }

    static marshaller = {
      JSON.registerObjectMarshaller(Question, 1) { q ->
        def possibilities  = q.possibilities.grep {
          it.active
        }.sort {
          it.sortOrder
        }.collect {
          [
            id: it.id,
            possibility: it.possibility
          ]
        }

        if(q.unknownPossibility) {
          possibilities.add([id: 0, possibility: 'I don\'t know/Not applicable'])
        }


        return [
          id: q.id,
          multivalued: q.multivalued,
          question: q.question,
          help: q.help,
          explanation: q.explanation,
          possibilities: possibilities
        ]
      }
    }

    String toString() {
      question
    }

}

