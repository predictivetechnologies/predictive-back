package com.predictive

import org.joda.time.DateTime

class UserResponse {
    User user
    Url url

    Boolean finished = false

    DateTime dateCreated
    DateTime lastUpdated

    static constraints = {
      url unique: 'user'
    }
}

