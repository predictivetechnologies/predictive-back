package com.predictive

class User {
    String username
    String passwordHash

    String facebookId
    String facebookOauth

    static hasMany = [ roles: Role, permissions: String ]

    static constraints = {
        username(nullable: false, blank: false, unique: true)
        passwordHash(nullable: true)
        facebookId(nullable: true)
        facebookOauth(nullable: true)
    }

    static mapping = {
        table 'person'
    }
}
