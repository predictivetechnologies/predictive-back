package com.predictive

class UserFeedBack {

  User user

  String name
  String email
  String subject
  String body

  static mapping = {
    name type: 'text'
    email type: 'text'
    subject type: 'text'
    body type: 'text'
  }
}
