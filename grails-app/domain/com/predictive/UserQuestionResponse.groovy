package com.predictive

import org.joda.time.DateTime

class UserQuestionResponse {

    static hasMany = [possibilities: Possibility]

    Question question
    User user
    UserResponse userResponse
    Boolean dontKnow

    DateTime dateCreated
    DateTime lastUpdated

    static constraints = {
      question unique: 'userResponse'
    }
}
