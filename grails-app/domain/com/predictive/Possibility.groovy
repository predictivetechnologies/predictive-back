package com.predictive

import org.joda.time.DateTime

class Possibility {

    static belongsTo = [question: Question]

    Boolean active = true
    String possibility
    Integer sortOrder

    DateTime dateCreated
    DateTime lastUpdated

    static mapping = {
      possibility type: 'text'
      sort sortOrder: 'asc'
    }

    String toString() {
      possibility
    }

}
