package com.predictive

import grails.converters.JSON

import org.joda.time.DateTime

class Url {

    Boolean active = true
    Boolean evaluated
    String url

    DateTime dateCreated
    DateTime lastUpdated

    static constraints = {
    }

    static marshaller = {
      JSON.registerObjectMarshaller(Url, 1) { u ->
        [
          url: u.url
        ]
      }
    }

    String toString() {
      url
    }
}

