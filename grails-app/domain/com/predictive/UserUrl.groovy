package com.predictive

import org.joda.time.DateTime

class UserUrl {

    User user
    Url url

    DateTime dateFinished

    DateTime dateCreated
    DateTime lastUpdated

    static constraints = {
      dateFinished nullable: true
    }
}
