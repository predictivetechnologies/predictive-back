package com.predictive

import org.apache.shiro.authc.AccountException
import org.apache.shiro.authc.IncorrectCredentialsException
import org.apache.shiro.authc.UnknownAccountException
import org.apache.shiro.authc.SimpleAccount
import org.apache.shiro.authz.permission.WildcardPermission

import com.restfb.*

class FacebookRealm {
    static authTokenClass = com.predictive.auth.FacebookToken

    def credentialMatcher
    def shiroPermissionResolver

    def authenticate(authToken) {
      try {
        def facebookClient = new DefaultFacebookClient(authToken.oauthToken)
        def userData = facebookClient.fetchObject("me", com.restfb.types.User.class)
        def username = userData.username

        def user = User.findByUsername(username)

        if(!user) {
          user = new User(username: username, facebookId: authToken.userId, facebookOauth: authToken.oauthToken)
          user.addToRoles(Role.findByName("Respondent"))
          user.save(failOnError: true)
        }

        def account = new SimpleAccount(username, authToken.oauthToken, "FacebookRealm")

        return account

      } catch(com.restfb.exception.FacebookGraphException ex) {
        throw new UnknownAccountException(ex.errorMessage)
      } catch(Exception ex) {
        ex.printStackTrace()
      }
      throw new UnknownAccountException("No account found for user")

    }

    def hasRole(principal, roleName) {
      def roles = User.withCriteria {
          roles {
              eq("name", roleName)
          }
          eq("username", principal)
      }

      return roles.size() > 0
    }

    def hasAllRoles(principal, roles) {
      def r = User.withCriteria {
          roles {
              'in'("name", roles)
          }
          eq("username", principal)
      }

      return r.size() == roles.size()
    }

    def isPermitted(principal, requiredPermission) {
      // Does the user have the given permission directly associated
      // with himself?
      //
      // First find all the permissions that the user has that match
      // the required permission's type and project code.
      def user = User.findByUsername(principal)
      def permissions = user.permissions

      // Try each of the permissions found and see whether any of
      // them confer the required permission.
      def retval = permissions?.find { permString ->
          // Create a real permission instance from the database
          // permission.
          def perm = shiroPermissionResolver.resolvePermission(permString)

          // Now check whether this permission implies the required
          // one.
          if (perm.implies(requiredPermission)) {
              // User has the permission!
              return true
          }
          else {
              return false
          }
      }

      if (retval != null) {
          // Found a matching permission!
          return true
      }

      // If not, does he gain it through a role?
      //
      // Get the permissions from the roles that the user does have.
      def results = User.executeQuery("select distinct p from User as user join user.roles as role join role.permissions as p where user.facebookId = '$principal'")

      // There may be some duplicate entries in the results, but
      // at this stage it is not worth trying to remove them. Now,
      // create a real permission from each result and check it
      // against the required one.
      retval = results.find { permString ->
          // Create a real permission instance from the database
          // permission.
          def perm = shiroPermissionResolver.resolvePermission(permString)

          // Now check whether this permission implies the required
          // one.
          if (perm.implies(requiredPermission)) {
              // User has the permission!
              return true
          }
          else {
              return false
          }
      }

      if (retval != null) {
          // Found a matching permission!
          return true
      }
      else {
          return false
      }
    }
}
