package com.predictive.auth

import org.apache.shiro.authc.*

class FacebookToken implements HostAuthenticationToken, RememberMeAuthenticationToken {
  String userId
  String host
  String oauthToken
  Boolean rememberMe = false

  public FacebookToken(String userId, String oauthToken, String host) {
    this.userId = userId
    this.host = host
    this.oauthToken = oauthToken
  }

  public FacebookToken(String userId, String oauthToken) {
    this(userId, oauthToken, null)
  }

  public Object getPrincipal() {
    userId
  }

  public Object getCredentials() {
    oauthToken
  }

  public boolean isRememberMe() {
    rememberMe
  }

}
